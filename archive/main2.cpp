#include <Arduino.h>
#include <Wire.h>
#include <LiquidCrystal.h>

#define MCP79410_I2C_ADDR 0x6f

// Initialize display
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

// Display setup
int displayColumns = 20;
int displayRows = 4;

// Forward function definition
void writeTime(byte second,
               byte minute,
               byte hour,
               byte dayOfWeek,
               byte dayOfMonth,
               byte month,
               byte year);

void setup() {
	Serial.begin(9600);
	Wire.begin();
	lcd.begin(displayColumns, displayRows);
	// writeTime(30,42,16,5,13,10,16);
	writeTime(0,0,0,1,1,1,20);
}


// Convert normal decimal numbers to binary coded decimal
byte dec2hex(byte val) {
	return((val / 10 * 16) + (val % 10));
}

// Convert binary coded decimal to normal decimal numbers
byte hex2dec(byte val) {
	return((val / 16 * 10) + (val % 16));
}

void writeTime(byte second,
               byte minute,
               byte hour,
               byte dayOfWeek,
               byte dayOfMonth,
               byte month,
               byte year) {
	Wire.beginTransmission(MCP79410_I2C_ADDR);
	Wire.write(0); // set next input to start at the seconds register
	Wire.write(dec2hex(second)); // set seconds
	Wire.write(dec2hex(minute)); // set minutes
	Wire.write(dec2hex(hour)); // set hours
	Wire.write(dec2hex(dayOfWeek)); // set day of week (1=Sunday, 7=Saturday)
	Wire.write(dec2hex(dayOfMonth)); // set date (1 to 31)
	Wire.write(dec2hex(month)); // set month
	Wire.write(dec2hex(year)); // set year (0 to 99)
	Wire.endTransmission();
}

void readTime(byte *second,
              byte *minute,
              byte *hour,
              byte *dayOfWeek,
              byte *dayOfMonth,
              byte *month,
              byte *year) {
	Wire.beginTransmission(MCP79410_I2C_ADDR);
	Wire.write(0); // set DS3231 register pointer to 00h
	Wire.endTransmission();
	Wire.requestFrom(MCP79410_I2C_ADDR, 1);

	// request seven bytes of data from DS3231 starting from register 00h
	*second = hex2dec(Wire.read() & 0x7f);
	*minute = hex2dec(Wire.read());
	*hour = hex2dec(Wire.read() & 0x3f);
	*dayOfWeek = hex2dec(Wire.read());
	*dayOfMonth = hex2dec(Wire.read());
	*month = hex2dec(Wire.read());
	*year = hex2dec(Wire.read());
}

void displayTime() {
	byte second, minute, hour, dayOfWeek, dayOfMonth, month, year;

	// Read data from RTC
	readTime(&second,
	         &minute,
	         &hour,
	         &dayOfWeek,
	         &dayOfMonth,
	         &month,
	         &year);

	// send it to the serial monitor
	Serial.print(hour, DEC);

	// convert the byte variable to a decimal number when displayed
	Serial.print(":");
	if (minute < 10) {
		Serial.print("0");
	}
	Serial.print(minute, DEC);
	Serial.print(":");
	if (second < 10) {
		Serial.print("0");
	}
	Serial.print(second, DEC);
	Serial.print(" ");
	Serial.print(dayOfMonth, DEC);
	Serial.print("/");
	Serial.print(month, DEC);
	Serial.print("/");
	Serial.print(year, DEC);
	Serial.print(" Day of week: ");
	switch(dayOfWeek) {
	case 1:
		Serial.println("Sunday");
		break;
	case 2:
		Serial.println("Monday");
		break;
	case 3:
		Serial.println("Tuesday");
		break;
	case 4:
		Serial.println("Wednesday");
		break;
	case 5:
		Serial.println("Thursday");
		break;
	case 6:
		Serial.println("Friday");
		break;
	case 7:
		Serial.println("Saturday");
		break;
	}
}
void loop(){
	displayTime(); // display the real-time clock data on the Serial Monitor,
	Serial.print("\n");
	delay(1000); // every second
}
